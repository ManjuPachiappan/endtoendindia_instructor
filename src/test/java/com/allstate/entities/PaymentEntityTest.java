package com.allstate.entities;
        import com.allstate.entities.Payment;
        import org.junit.jupiter.api.Test;
        import java.util.Date;
        import static org.junit.jupiter.api.Assertions.assertEquals;

public class PaymentEntityTest {
    Date paymentDate = new Date();
    String type = "Visa";
    double amount = 2000.0;
    int custId = 1;
    int id = 1;

    @Test
    public void constructorInitializesPaymentObjectWithExpectedValues() {

        Payment payment = new Payment(id, paymentDate, type, amount, custId);

        assertEquals(id, payment.getId());
        assertEquals(paymentDate, payment.getPaymentDate());
        assertEquals(type, payment.getType());
        assertEquals(amount, payment.getAmount());
        assertEquals(custId, payment.getCustId());
    }

    @Test
    public void toStringReturnsExpectedValue() {

        Payment payment = new Payment(id, paymentDate, type, amount, custId);

        String expectedValue = "Payment{ id = " + id + ", paymentDate = " + paymentDate + ", type = " + type + ", amount = " + amount + ", custId = " + custId + '}';

        assertEquals(expectedValue,  payment.toString());
    }
}
