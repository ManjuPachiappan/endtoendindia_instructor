package com.allstate.dao;

import com.allstate.entities.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doReturn;
import static org.mockito.MockitoAnnotations.initMocks;

public class PaymentDaoMock {
    Date paymentDate = new Date();
    String type = "Visa";
    double amount = 2000.0;
    int custId = 1;
    int id = 1;
    Payment payment;
    @Mock
    private PaymentDao dao;

    @BeforeEach
    void setUp() {
        initMocks(this);
         payment = new Payment(id, paymentDate, type, amount, custId);
    }


    @Test
    void rowCount_success() {
        doReturn(1L).when(dao).rowcount();
        assertEquals(1L, dao.rowcount());
    }

    @Test
    void findById_success() {

         doReturn(payment).when(dao).findByID(anyInt());
        assertEquals(id, payment.getId());
        assertEquals(paymentDate, payment.getPaymentDate());
        assertEquals(type, payment.getType());
        assertEquals(amount, payment.getAmount());
        assertEquals(custId, payment.getCustId());
    }

    @Test
    void findByType_success() {
        List<Payment> lst = new ArrayList<Payment>();
        lst.add(payment);
        doReturn(lst ).when(dao).findByType(any());
        assertTrue(lst.size()>0, "List type returned");

    }

    @Test
    void save_success() {
        Date date = new Date();
        int result=1;
        doReturn(result).when(dao).save(any(Payment.class));
        Payment newpayment = new Payment(id, date, type, amount, custId);

        assertEquals(result, dao.save(newpayment));

    }





}
