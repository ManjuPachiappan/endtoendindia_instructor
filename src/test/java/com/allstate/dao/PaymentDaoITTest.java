package com.allstate.dao;

import com.allstate.dao.PaymentDao;
import com.allstate.entities.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class PaymentDaoITTest {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private PaymentDao paymentDao;

    @BeforeEach
    public void cleanupAndReset() {
        mongoTemplate.dropCollection(Payment.class);

        mongoTemplate.insert(new Payment(1, new Date(), "coin", 100.00, 100));
        mongoTemplate.insert(new Payment(2, new Date(), "visa", 200.00, 101));
        mongoTemplate.insert(new Payment(3, new Date(), "cash", 300.00, 102));
        mongoTemplate.insert(new Payment(4, new Date(), "visa", 500.00, 103));

    }

    @Test
    public void rowCountReturnsCountOfPaymentsInCollection() {


        assertEquals(4, paymentDao.rowcount());
    }

    @Test
    public void findByIdReturnsExpectedPayment() {


        Payment foundPayment = paymentDao.findByID(1);

        assertNotNull(foundPayment);
        assertEquals(1, foundPayment.getId());
    }

    @Test
    public void findByTypeReturnsAllMatchingPayments() {


        List<Payment> payments = paymentDao.findByType("visa");

        assertEquals(2, payments.size());
    }

    @Test
    public void saveInsertsPaymentIntoTheDatabase() {

       int result= paymentDao.save(new Payment(5, new Date(), "cash", 100.00, 105));

        assertEquals(1, result);
    }
}
