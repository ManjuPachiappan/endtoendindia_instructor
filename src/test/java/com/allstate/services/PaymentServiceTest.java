package com.allstate.services;


import com.allstate.dao.PaymentDao;
import com.allstate.dao.PaymentImpl;
import com.allstate.entities.Payment;
import com.allstate.exceptions.PaymentException;
import com.allstate.service.PaymentService;
import com.allstate.service.PaymentServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class PaymentServiceTest {


    @Autowired
    PaymentService service;

    Payment payment;


    @BeforeEach
    void setUp() {
        Date paymentDate = new Date();
        String type = "Visa";
        double amount = 2000.0;
        int custId = 1;
        int id = 1;
        payment= new Payment(id, paymentDate, type, amount, custId);
    }



    @Test
    public void savePayment(){
            int returnValue = service.save(payment);
        assertEquals(1, service.rowcount());

    }


    @Test
    void rowCount_success() {

        long result=service.rowcount();
        assertTrue(result >1,"Payment returned by id");


    }

    @Test
    void findById_success() {

        Payment payment = service.findById(1);
        assertTrue(payment.getId() == 1,"Payment returned by id");

    }



    @Test
    void findById_exception() {
        Throwable exception =
                assertThrows(PaymentException.class, () -> {
                    service.findById(-1);
                });
        assertEquals("The id value must be greater than zero.", exception.getMessage());
    }


    @Test
    void save_exception() {
        Throwable exception =
                assertThrows(PaymentException.class, () -> {
                    service.save(new Payment());
                });
        assertEquals("Invalid Data", exception.getMessage());
    }


}

