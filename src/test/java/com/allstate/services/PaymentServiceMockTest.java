package com.allstate.services;

import com.allstate.dao.PaymentDao;
import com.allstate.entities.Payment;
import com.allstate.service.PaymentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.MockitoAnnotations.initMocks;

@SpringBootTest
public class PaymentServiceMockTest {
    @Autowired
    PaymentService service;


    @MockBean
    private PaymentDao dao;


    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    public void rowCountTest() {
        doReturn(1).when(dao).rowcount();
        assertEquals(1, service.rowcount());
    }


    @Test
    public void savePaymentSuccessTest() {
        Date paymentDate = new Date();
        String type = "Visa";
        double amount = 2000.0;
        int custId = 1;
        int id = 1;
        Payment payment= new Payment(id, paymentDate, type, amount, custId);

         doReturn(1).when(dao).save(payment);
        assertEquals(1, service.save(payment));
    }
}
