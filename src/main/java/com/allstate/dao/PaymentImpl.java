package com.allstate.dao;

import com.allstate.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PaymentImpl implements PaymentDao {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public long rowcount() {
     //   List<Payment> paymentList = mongoTemplate.findAll(Payment.class);
     //   return paymentList.size();  //int

        return mongoTemplate.count(new Query(), Payment.class);
    }

    @Override
    public Payment findByID(int id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Payment payment = mongoTemplate.findOne(query, Payment.class);
        return payment;
    }

    @Override
    public List<Payment> findByType(String type) {

        Query query = new Query();
        query.addCriteria(Criteria.where("type").is(type));
        return mongoTemplate.find(query, Payment.class);

    }

    @Override
    public int save(Payment payment) {
        Payment payment1= mongoTemplate.insert(payment);
        if (payment1 !=null)
        {return 1;}
        else
        {return 0;}
    }

    @Override
    public List<Payment> getAllPayments() {
        return mongoTemplate.findAll(Payment.class);
    }

    @Override
    public int update(Payment payment) {


        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(payment.getId()));
        Update update = new Update();
        update.set("type", payment.getType());
        update.set("amount", payment.getAmount());
        update.set("custId", payment.getCustId());

        mongoTemplate.updateFirst(query, update, Payment.class);
        return 0;
//        mongoTemplate.update(Payment.class).
//                matching(query(where("id").is(payment.getId()))).replaceWith(payment)))
    }
}
