package com.allstate.service;

import com.allstate.dao.PaymentDao;
import com.allstate.entities.Payment;
import com.allstate.exceptions.InvalidPaymentRequestException;
import com.allstate.exceptions.PaymentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentServiceImpl implements PaymentService {
    @Autowired
    private PaymentDao dao;


    @Override
    public long rowcount() {
        return dao.rowcount();
    }

    @Override
    public Payment findById(int id) {

        if(id > 0) {
            return dao.findByID(id);
        }
        else {
            throw new IllegalArgumentException("The id value must be greater than zero.");
        }


    }

    @Override
    public List<Payment> findByType(String type) {


        if(type != null) {
            return dao.findByType(type);
        }
        else {
            throw new IllegalArgumentException("The type parameter cannot be null.");
        }


    }

    @Override
    public int save(Payment payment) throws PaymentException {

            if (validate(payment)) {
                return dao.save(payment);

            } else {
                throw new InvalidPaymentRequestException("Invalid Data");


            }
        }


    @Override
    public List<Payment> getAll() {
        return dao.getAllPayments();
    }


    @Override
    public int update(Payment payment) {
        return dao.update(payment);
    }


    public static boolean validate(Payment payment) {
        return payment.getId() > 0
                && payment.getPaymentDate() != null
                && payment.getType() != null
                && payment.getAmount() > 0.0
                && payment.getCustId() > 0;
    }

}
