package com.allstate.service;

import com.allstate.entities.Payment;
import com.allstate.exceptions.PaymentException;

import java.util.List;

public interface PaymentService {

    long rowcount();
    Payment findById(int id);
    List<Payment> findByType(String type);
    int save(Payment payment) throws PaymentException;
    List<Payment> getAll();
    int update(Payment payment);
}
